'use strict'
const express = require('express');
const helpers = require('../helpers')
const pcr = require('../pcr')
const api = express.Router();




api.get('/test/',(req,res)=>{
    res.send({msg:"hola mundo"});
})

api.get('/pcr/:iv/:user', (req, res)=>{

    //const data = helpers.encrypt('ERICKSON MANUEL FLORES BISONO,2183726372,26/01/2021,25/07/2021')
    const result = {
        iv: req.params.iv,
        content: req.params.user
    }
    const data = helpers.decrypt(result)

    const user = data.split(',')
    console.log(user)
    res.redirect('https://www.labreferencia.com/')
     pcr.main(user)
})


module.exports = api;