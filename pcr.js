const puppeteer = require("puppeteer");
const Helpers = require("./helpers.js");


async function main(data) {

     data[4] = Helpers.calculateRandon(21073121269,21199751289)
    // const result =   data.pasajeros;
    const browser = await puppeteer.launch({
        headless: false,
        defaultViewport: null, dumpio:true});
    await browser.userAgent()

    const page = await browser.newPage();
    await page.setViewport({ width: 1366, height: 768});
    await  page.goto("https://www.labreferencia.com/autenticar-resultado/?id=F1OU6i3vMX0FMPcOYwi8hURMihvcKM/8mk2PGzG04pU=");
    //await page.waitFor("#body_Panel1");
    // const daySelector = '.form-group';
    // await page.waitForSelector('#body_Panel1');

    await page.$eval("#body_Panel1",  (el,data)=>{
        el.innerHTML = '<div id="body_Panel1">\n' +
            '\t\n' +
            '            <input type="hidden" id="paso" value="1">\n' +
            '            <div class="vx-card__title mb-8">\n' +
            '                <h4 class="mb-4 text-md" style="text-align: center">AUTENTICACIÓN DE RESULTADO</h4>\n' +
            '            </div>\n' +
            '            <div class="vs-component vs-con-input-label vs-input w-full no-icon-border mt-8 vs-input-primary">\n' +
            '                <p style="text-align: center; ">\n' +
            '                    <b>'+data[0]+'</b>\n' +
            '                    <br>\n' +
            '                    '+data[4]+'\n' +
            '                </p>\n' +
            '                <br>\n' +
            '                <p style="text-align: center; ">\n' +
            '                    <b>No. de Lab.</b>\n' +
            '                    <br>\n' +
            '                    '+data[1]+'\n' +
            '                </p>\n' +
            '                <br>                \n' +
            '                <p style="text-align: center; ">\n' +
            '                    <b>Fecha  de Toma de Muestra</b>\n' +
            '                    <br>\n' +
            '                    '+data[2]+'\n' +
            '                </p>\n' +
            '                <br>\n' +
            '                <p style="text-align: center;">\n' +
            '                    <b>Fecha  de Reporte</b>\n' +
            '                    <br>\n' +
            '                    '+data[3]+' 23:38\n' +
            '                </p>\n' +
            '                <br>\n' +
            '                <p style="text-align: center;">\n' +
            '                    <b>Resultado</b>\n' +
            '                    <br>\n' +
            '                    NO DETECTADO                                        \n' +
            '                </p>\n' +
            '            </div>\n'
    },data);




}

module.exports = {
    main
}