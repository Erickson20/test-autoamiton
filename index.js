'use strict'

const app = require('./app');
const config = require('./config')
const cors = require('cors')


app.use(cors({
    'allowedHeaders': ['sessionId', 'Content-Type'],
    'exposedHeaders': ['sessionId'],
    'origin': '*',
    'methods': 'GET,HEAD,PUT,PATCH,POST,DELETE',
    'preflightContinue': false
}));

const server = app.listen(config.port || '3000', () => {
    console.log('Server iniciado correctamente en el puerto'+config.port);
})
app.on('listening',function(){
    console.log('ok, server is running');
    server.close();
});

//server.close();
